# TT Mainpalette library by Tritem

<!-- ## General information

_placeholder: to be added in the future_ -->

## Dependency

This project has no dependencies.

## Notes

### Automatic installation

Using _Chocolatey_ - versions up to `1.4.0`

`choco install tt-mainpalette -s "'https://gitlab.com/api/v4/projects/51816800/packages/nuget/v2'"`

Using _Chocolatey_ - versions from `2.0.0`

`choco install tt-mainpalette -s "'https://gitlab.com/api/v4/projects/51816800/packages/nuget/index.json'"`

or

`choco install tt-mainpalette -s "'https://gitlab.com/api/v4/groups/74269373/-/packages/nuget/index.json'"`

### Manual installation

Go to `https://gitlab.com/tt_libraries/tt-mainpalette/-/packages` and download the latest version of package. Then, using command prompt:

```powershell
cd <path-to-folder-with-downloaded-package>
choco install tt-mainpalette -s .
```

Use `-f` flag to force installation procedure.

Installation script **overwrites** content of `menus\Categories\Tritem\dir.mnu`!

Uninstall procedure **removes** `menus\Categories\Tritem\dir.mnu`!
